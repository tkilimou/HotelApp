> Use GitLab CI/CD to catch bugs and errors early in the development cycle. Ensure that all the code deployed to production complies with the code standards you established for your app.
>
> GitLab CI/CD can automatically build, test, deploy, and monitor your applications by using [Auto DevOps](https://docs.gitlab.com/ee/topics/autodevops/index.html).
>
> For a complete overview of these methodologies and GitLab CI/CD, read the [Introduction to CI/CD with GitLab](https://docs.gitlab.com/ee/ci/introduction/index.html). 

# Setup your first Gitlab CI pipeline
1. **enable CI/CD pipelines**: in « *YOUR_PROJECT* -> Settings -> General ->
  Permissions -> Repository » enable the « Pipeline » switch

2. **enable a QLF-CI runner**: A runner is needed to build jobs that you will 
  define in the gitlab CI configuration file. Inria provides runner you can use for
  your projects. To use them, in « *YOUR_PROJECT* -> Settings -> CI/CD ->
  Runners » the left column lists the configured and available gitlab specific
  runners. Choose the most suitable runner for your project (INRIA QLF-CI
  small, medium or large) and click « Enable for this project »<br>
  **IMPORTANT: if you wish to remove the runner from your project, please
  DO NOT click on the ```Pause``` button, but use ```Disable for this
  project```** (hitting ```Pause``` would disable the runner for everybody)
  
3. **provide a `.gitlab-ci.yml` configuration file**: this file must be stored
  at the root of your project's git repository. The minimum requirement is to
  provide a docker image and a command (see the example below). For more
  complex usecases, you should read the [documentation of the docker executor
  of gitlab-runner](https://docs.gitlab.com/runner/executors/docker.html).

```yaml
---
job1:
    image: busybox
    script: 
     - echo "Hello World"
     - echo "I am in a good mood today"
```

Once done, gitlab will schedule the job each time a commit is pushed to your
repository. The status CI jobs is visible in the  « CI/CD » tab of your
project.

# Update Gitlab CI pipeline to build the project with maven
The build command is `mvn verify`.
Hint: you will need to update the docker image by installing required package(s),
for example with apt. Another solution is to use a pre-built image like `maven:3-openjdk-11`.

Introduction to Gitlab CI / CD pipelines and jobs: https://docs.gitlab.com/ee/ci/pipelines.html

# Useful links
- Quick start: https://docs.gitlab.com/ee/ci/quick_start/.
- Gitlab CI file syntax: https://docs.gitlab.com/ee/ci/yaml/gitlab_ci_yaml.html
- Use docker images to build jobs: It is easier to use Docker container to build your job. For example, you can use the `maven:latest`image (see https://docs.gitlab.com/ee/ci/docker/using_docker_images.html#define-image-in-the-gitlab-ciyml-file).

